# Code to emulator system log and retrieve packet tag info
# And plot the number of packets per app
# We used Anaconda's distribution of python

%matplotlib inline
import pandas as pd
import numpy as np
from pandas import Series, DataFrame

import matplotlib.pyplot as plt
import matplotlib as mpl
import time
from drawnow import drawnow
import pylab as pl
from IPython import display
from collections import defaultdict
import subprocess as sub

# Fetch and parse data from emulator system log
def fetch(last_count, apps):
    p = sub.Popen(['adb','shell','dmesg','|','grep','ec521start'],stdout=sub.PIPE,stderr=sub.PIPE)
    output, errors = p.communicate()
    str_list = output.splitlines()

    new_last_count = last_count   
    for str in str_list:
        pieces = str.split("#")
        count = int(pieces[1])    
        if count > new_last_count:
            new_last_count = count
        if count > last_count:            
            apps[pieces[2]] += 1
        
    print errors
    return new_last_count

# plot graph
last_count = 0
apps = defaultdict(int) # app => count
apps['facebook'] = 0
apps['opera'] = 0
apps['browser'] = 0
apps['instagram'] = 0
apps['twitter'] = 0

loops = 0
for i in range(1000):
    loops += 1
    last_count = fetch(last_count, apps)
    
    if loops % 5 == 0: # refresh plot every 5 seconds
        aframe = pd.DataFrame.from_dict(apps, orient='index', dtype=None)
        aframe.columns = ['packets']
        x1,x2,y1,y2 = plt.axis()
        plt.axis((x1,x2,0,150))
        aframe.packets.plot(kind='bar', figsize=(12, 8), color='slategray', title='No. of packets per app')

        display.clear_output(wait=True)
        display.display(pl.gcf())
    
    time.sleep(1.0)


