// Code from: http://www.paulkiddie.com/2009/11/creating-a-netfilter-kernel-module-which-filters-udp-packets/ 
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/udp.h>
#include <linux/ip.h>

static struct nf_hook_ops nfho;   //net filter hook option struct
struct udphdr *udp_header;          //udp header struct (not used)
struct iphdr *ip_header;            //ip header struct

unsigned int hook_func(unsigned int hooknum, struct sk_buff *skb, const struct net_device *in, const struct net_device *out, int (*okfn)(struct sk_buff *))
{
	struct iphdr *saved_ip_header;
	unsigned char *ip_opt; 
	int tot_option_size;
	char *app_name;  

	printk(KERN_WARNING "In strip.\n");  

	//grab network header using accessor
        ip_header = (struct iphdr *)skb_network_header(skb);   

	// Get the start address of the options 
	unsigned char ihl = ip_header->ihl; 
	int calc_ihl = ihl;

	if (calc_ihl == 5) {
		// No options present. Return 
		return NF_ACCEPT; 
	}
	
	ip_opt = (unsigned char *)(((void*)ip_header) + 20);

	// Get the total options size 
	tot_option_size = (int) *(ip_opt + 1);
	printk (KERN_WARNING "\tOptions size = %d\n", tot_option_size);
	app_name = kmalloc(tot_option_size - 3 + 1, 0);
	// 3 is the first 3 bytes of the options, which are not "data" for us
	memcpy(app_name, ip_opt + 3, tot_option_size - 3);  	 
	app_name[tot_option_size - 3] = '\0';
	printk(KERN_WARNING "<Recovered name start>%s<Recovered name end>\n", app_name);
        
	return NF_ACCEPT;
}
 
int init_module()
{
        nfho.hook = hook_func;
	// Type of hook 
        nfho.hooknum = NF_INET_POST_ROUTING;
        // Protocol family 
	nfho.pf = PF_INET;
	// Type of priority. Hooks with higher priority get their callbacks serviced first 
        nfho.priority = NF_IP_PRI_FIRST;
 
        nf_register_hook(&nfho);
       
        return 0;
}
 
void cleanup_module()
{
        nf_unregister_hook(&nfho);     
}

