// Code from: http://www.paulkiddie.com/2009/11/creating-a-netfilter-kernel-module-which-filters-udp-packets/ 
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/udp.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/fs.h>      // Needed by filp
#include <asm/uaccess.h>   // Needed by segment descriptors

static struct nf_hook_ops nfho;   //net filter hook option struct
struct udphdr *udp_header;          //udp header struct (not used)
struct iphdr *ip_header;            //ip header struct
struct appName_uid **appName_uid_arr;
int size_dict; 

struct appName_uid {
  char* appName;
  int uid;
//  char* uid; 
};

void my_free0(struct appName_uid** appName_uid_arr, int length);
int storeUIDPairs(void);
int get_line(char* input, char *buffer, size_t buflen);
int parseAndStore(char* source, int fileSize, int max_lineLen, struct appName_uid **appName_uid_arr);
int find(struct appName_uid** appName_uid_arr, int uid, int length);
int translate(int uid, char **app_name);

/*
struct iphdr {
__u8    tos
__u16   tot_len
__u16   id
__u16   frag_off
__u8    ttl
__u8    protocol
__u16   check
__u32   saddr
__u32   daddr
}*/

void printIphdr(struct iphdr* iph) 
{
	char source[16];
	char dest[16]; 
	printk(KERN_WARNING "\t------------IPHDR FIELDS-------------\n"); 
        printk(KERN_WARNING "\tip_header ptr = %p\n", iph);
	printk(KERN_WARNING "\tip_header->ihl = %d\n", iph->ihl);
	printk(KERN_WARNING "\tip_header->version = %d\n", iph->version); 
        printk(KERN_WARNING "\tip_header->tos = %d\n", iph->tos);
        printk(KERN_WARNING "\tip_header->tot_len = %hu\n", iph->tot_len);        
	printk(KERN_WARNING "\tip_header->id = %hu\n", iph->id);
        printk(KERN_WARNING "\tip_header->frag_off = %d\n", iph->frag_off);
	printk(KERN_WARNING "\tip_header->ttl = %d\n", iph->ttl); 
	printk(KERN_WARNING "\tip_header->protocol = %d\n", iph->protocol); 
	printk(KERN_WARNING "\tip_header->check = %hu\n", iph->check); 
        snprintf(source, 16, "%pI4", &(iph->saddr)); // Mind the &!
        printk(KERN_WARNING "\tHeader source addr = %s\n", source); 
        snprintf(dest, 16, "%pI4", &(iph->daddr)); // Mind the &!
        printk(KERN_WARNING "\tHeader dest addr = %s\n", dest);
	printk(KERN_WARNING "\t------------[END] IPHDR FIELDS-------------\n\n");
}

int translate(int uid, char **app_name) 
{
	switch (uid) {
	case 10017:
		*app_name = "browser"; 
		return 7; 
	case 10027:
		*app_name = "email"; 
		return 5;
	case 10054:
		*app_name = "facebook";
		return 8;  
	default: 
		*app_name = "undefined";
		return 9; 
	}
}

int find(struct appName_uid** appName_uid_arr, int uid, int length)
{
  int i;
  for (i = 0; i < length; i++) {
    if (appName_uid_arr[i]) {
	if (uid == appName_uid_arr[i]->uid) {
        	return i;
      }
    }
  }
  return -1;
}

/*
void my_free0(struct appName_uid** appName_uid_arr, int length)
{
  int i;
  for (i = 0; i < length; i++) {
    if (appName_uid_arr[i]) {
      free(appName_uid_arr[i]->appName);
      free(appName_uid_arr[i]);
    }
  }
  free(appName_uid_arr);
}
*/

int storeUIDPairs(void) 
{
    // Create variables
    struct file *f;
    int filesize = 1550; 
    char buf[filesize];
    int max_lineLen = 80; 
    mm_segment_t fs;
    int i;

    // Init the buffer with 0
    for(i = 0; i < filesize; i++)
        buf[i] = 0;

    // Open file 
    // /data/system/packages.list
    f = filp_open("test.txt", O_RDONLY, 0);

    // Read file 
    if (f == NULL)
        printk(KERN_ALERT "filp_open error!!.\n");
    else{
        // Get current segment descriptor
        fs = get_fs();
        // Set segment descriptor associated to kernel space
        set_fs(get_ds());
        // Read the file
        f->f_op->read(f, buf, filesize, &f->f_pos);
        // Restore segment descriptor
        set_fs(fs);
        // See what we read from file
        //printk(KERN_WARNING "buf:%s\n",buf);
    }

    // Parse buffer line by line and store in global struct  
    size_dict = parseAndStore(buf, filesize,  max_lineLen, appName_uid_arr);  
    
    // Close file 
    filp_close(f,NULL);

    // Return 
    return 0;
}

int parseAndStore(char* source, int fileSize, int max_lineLen, struct appName_uid **appName_uid_arr)
{
    char *startTag; 
    int startTag_len; 
    char *start; 
    char *end;
    char *start2;
    char *end2;
    int len;  
    int len2; 
    char eachLine[max_lineLen];
    char * line_beg = source;
    int line_bytesRead = 0;
    int total_bytesRead = 0;
    int lines = 0;
    char* strtol_arg; 
    while (total_bytesRead + max_lineLen < fileSize) { 
      // Retreive one line at a time
	printk (KERN_WARNING "HERE1\n");  
      line_bytesRead = get_line(line_beg, eachLine, max_lineLen);

          //printf("%s\n", eachLine);

          startTag = "com.android.";
          startTag_len = 12;

          start = strstr(eachLine, startTag);
          end = strstr(eachLine, " ");
          if (start && end) {
	    printk (KERN_WARNING "Parsing %s\n\n\n", eachLine); 
            start += startTag_len;
            len = end - start;
            char appName[len + 1];
            memcpy(appName, start, len);
            appName[len] = '\0';
            printk (KERN_WARNING "AppName = %s\n\n\n", appName); 

            start2 = strstr(start, " ") + 1;
            end2 = strstr(start2, " ");
            len2 = end2 - start2;
            char uid[len2 + 1];
            memcpy(uid, start2, len2);
            uid[len2] = '\0';
            printk(KERN_WARNING " UID = %s\n\n\n", uid); 

            appName_uid_arr[lines] = (struct appName_uid*) kmalloc(sizeof(struct appName_uid), 0);
            appName_uid_arr[lines]->appName = (char*) kmalloc(len, 0);
            memcpy(appName_uid_arr[lines]->appName, appName, len);
	    (appName_uid_arr[lines]->appName)[len] = '\0';
            appName_uid_arr[lines]->uid = simple_strtol(uid, &strtol_arg, 10); //atoi(uid);
	    printk (KERN_WARNING " STORED UID = %d\n\n\n", appName_uid_arr[lines]->uid); 
            //appName_uid_arr[lines]->uid = (char*) kmalloc(len2, 0);
	    /*if ( appName_uid_arr[lines]->uid == NULL) {
              printk (KERN_WARNING "HERE!!!!!!!\n\n\n\n\n\n\n"); 
            }*/
	    //printk (KERN_WARNING "HERE!\n"); 
            //memcpy(appName_uid_arr[lines]->uid, uid, len2);
	    printk (KERN_WARNING "HERE2!\n"); 
            //(appName_uid_arr[lines]->uid)[len2] = '\0';
            //printk( KERN_WARNING "Appname = %s UID = %d Address = %p\n", appName_uid_arr[lines]->appName, appName_uid_arr[lines]->uid, appName_uid_arr[lines]);
	    printk (KERN_WARNING "HERE3\n"); 
          }
         lines++;

      /**** CONTROL *****/

      //printf("%s\n", eachLine);
      // Move line beginning up to next line (ignore \n)
      line_beg += line_bytesRead + 1;
      // Update total bytes read counter
      total_bytesRead += line_bytesRead;
	printk (KERN_WARNING "Total bytes read = %d FIlesize = %d\n", total_bytesRead, fileSize); 
    }

   return lines;
}

int get_line(char* input, char *buffer, size_t buflen)
{
  char *end = buffer + buflen - 1; /* Allow space for null terminator */
  char *dst = buffer;
  int c;
  int count = 0;
  while ((c = *input++) != '\n' && dst < end) {
    *dst++ = c;
    count++;
  }

  *dst = '\0';
  return count;
}

void printSkbMemory(struct sk_buff *skb) 
{
	unsigned char *data = skb->data; 
	unsigned int length = skb->len; 
	int i; 
	
	printk(KERN_WARNING "\t----------------SKB MEMORY-------------\n");
	printk(KERN_WARNING "\tPrinting %d bytes\n", (int) length);  
	for (i = 0; i < (int) length; i+=8) {
               printk(KERN_WARNING "\t%p: %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x\n", data, *data, *(data + 1), *(data + 2), *(data + 3), *(data + 4), *(data + 5), *(data + 6), *(data + 7));
		data = ((void *) data) + 8; 
	}
	printk(KERN_WARNING "\t----------------[END] SKB MEMORY-------------\n\n");

}

/* Compute checksum for count bytes starting at addr, using one's complement of one's complement sum*/
unsigned short compute_checksum(unsigned short *addr, unsigned int count) {

	register unsigned long sum = 0;
	while (count > 1) {
		sum += * addr++;
		count -= 2;
	}

  	//if any bytes left, pad the bytes and add
  	if(count > 0) {
    		sum += ((*addr)&htons(0xFF00));
  	}

	//Fold sum to 16 bits: add carrier to result
 	while (sum>>16) {
      		sum = (sum & 0xffff) + (sum >> 16);
  	}
  
	//one's complement
  	sum = ~sum;
  	return ((unsigned short)sum);
}

/* set ip checksum of a given ip header*/
void compute_ip_checksum(struct iphdr* iphdrp){
        iphdrp->check = 0;
        iphdrp->check = compute_checksum((unsigned short*)iphdrp, iphdrp->ihl<<2);
}

unsigned int hook_func(unsigned int hooknum, struct sk_buff *skb, const struct net_device *in, const struct net_device *out, int (*okfn)(struct sk_buff *))
{
	struct iphdr *new_iph;
	unsigned char *new_ip_opt;
	struct tcphdr *tcp_hdr;
	int tot_option_size; 
	char *app_name; 
	int app_name_length; 

	printk (KERN_WARNING "------------------[BEGIN] HOOK_FUNC ----------------\n\n");
	// 1. Print BEFORE fields 
        ip_header = (struct iphdr *)skb_network_header(skb); 
        printSkbMemory(skb); 
	printIphdr(ip_header);  

	// 2. Only augment PSH (Data) packets 
	if (ip_header->protocol == 17) {
		printk(KERN_WARNING "\tNot tagging UDP Packets. Returning untouched. \n");
		printk (KERN_WARNING "------------------[END] HOOK_FUNC ----------------\n\n\n"); 
		return NF_ACCEPT; 
	}

        if (ip_header->protocol == 1) {
                printk(KERN_WARNING "\tNot tagging ICMP Packets. Returning untouched. \n");
		printk (KERN_WARNING "------------------[END] HOOK_FUNC ----------------\n\n\n");
                return NF_ACCEPT;
        }

	if (ip_header->protocol == 6) {
		tcp_hdr = (struct tcphdr *) skb_transport_header(skb); 
		if (!tcp_hdr->psh) {
			printk(KERN_WARNING "\tOnly tagging PSH Packets. Returning untouched.\n"); 
			printk (KERN_WARNING "------------------[END] HOOK_FUNC ----------------\n\n\n");
			return NF_ACCEPT;
		}
	}

 	tot_option_size = 40; 

	/*******************************************************************
	 Original: [HEAD ROOM][IP HEADER][TCP HEADER][USER DATA][TAIL ROOM]
		              ^--- ip_header		       
	*******************************************************************/ 

	// Here we proceed to augmenting the ip_header with "options"
	// http://www.skbuff.net/skbbasic.html
	// http://vger.kernel.org/~davem/skb_data.html 

	// 3. Allocate a new memory buffer of the IP header
	new_iph = (struct iphdr *)skb_push(skb, tot_option_size);
	printk(KERN_WARNING "\n\t-----------------MIDDLE------------------\n"); 
	printk(KERN_WARNING "\t-----------------MIDDLE------------------\n");
	printk(KERN_WARNING "\t-----------------MIDDLE------------------\n\n");

        /**********************************************************************************
	  New skbuf: [HEAD ROOM][X NEW BYTES][IP HEADER][TCP HEADER][USER DATA][TAIL ROOM]
	         		^----- new_iph
        **********************************************************************************/

	// 4. Copy the contents of previous IP header to the new one.
	memcpy(new_iph, ip_header, sizeof(struct iphdr));

	// 5. Now a assign the new IP header pointer to the local IP header pointer
	// Makes things easier to differentiate 
	ip_header = new_iph;

	// 6. Modifing the length and ihl field to new IP header
	ip_header->ihl = 5 + (tot_option_size / 4);
	ip_header->tot_len = ntohs(htons(ip_header->tot_len) + tot_option_size);

	// 7. Creating a local pointer to point the IP option field
	// Again, this makes things easier 
	new_ip_opt = (unsigned char *)(((void*)ip_header) + sizeof(struct iphdr));

	// 8. Adding data in the option field
	*(new_ip_opt + 0) = (unsigned char) 68; // IP Number 
        *(new_ip_opt + 1) = (unsigned char) tot_option_size; // Length
        *(new_ip_opt + 2) = (unsigned char) 17; // Pointer

	/* ***** ENCODING APPLICATION NAME ***** */
	struct sock *my_sock = skb->sk;
	struct socket *my_socket = my_sock->sk_socket;
	// TODO: llin. Add this line in on emulator system 
	//int uid = my_socket->uid; 
	int uid = 10054; 
	app_name_length = translate(uid, &app_name); 
	printk (KERN_WARNING "\tEncoding %s\n\n\n", app_name);  
	memcpy(new_ip_opt + 3, app_name, app_name_length);
	memset(new_ip_opt + 3 + app_name_length, 0, tot_option_size - (3 + app_name_length));  

	// 9. Setting network_header to new position 
	skb->network_header = ((void*) skb->network_header) - tot_option_size; 

	// 10. Compute IP Checksum
	compute_ip_checksum(ip_header);

	// 11. Print AFTER fields 
	printSkbMemory(skb);
	printIphdr((struct iphdr *)skb_network_header(skb));

	printk (KERN_WARNING "------------------[END] HOOK_FUNC ----------------\n\n\n"); 

	// 12. Return 
        if(!skb) { return NF_ACCEPT;}

        return NF_ACCEPT;
}
 
int init_module()
{

	/* ***** SETUP NETFILTER ***** */
        nfho.hook = hook_func;
	// Type of hook 
        nfho.hooknum = NF_INET_LOCAL_OUT;
        // Protocol family 
	nfho.pf = PF_INET;
	// Type of priority. Hooks with higher priority get their callbacks serviced first 
        nfho.priority = NF_IP_PRI_FIRST;
 
        nf_register_hook(&nfho); 

        return 0;
}
 
void cleanup_module(void)
{
        nf_unregister_hook(&nfho);    

}

